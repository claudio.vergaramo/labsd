package paquetes

import (
	context "context"
	"encoding/csv"
	"log"
	"math/rand"
	"os"
	"time"
)

type Server struct {
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func (s *Server) ListaEnvio(ctx context.Context, message *PaqueteLogistica) (*PaqueteLogistica, error) {
	ochentaporciento := [10]int{1, 1, 1, 1, 1, 1, 1, 1, 0, 0}

	//fmt.Printf("%d\n", ochentaporciento[rand.Intn(9)])

	var timestamp string

	for i := 0; i < 2; i++ {
		numrand := ochentaporciento[rand.Intn(9)]

		if numrand == 1 {
			t := time.Now()
			timestamp = t.Format("2006-01-02 15:04:05")

			message.Estado = "Recibido"
			message.Intento = "1"
			message.Hora = timestamp
			break
		} else {
			if i == 0 {
				message.Intento = "2"
			} else {
				message.Intento = "2"
				message.Estado = "No Recibido"
			}

		}
	}

	file, err := os.OpenFile("../logistica/registro_camiones.csv", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	checkError("Cannot create file", err)
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	var data = [][]string{{message.IDPaquete, message.Tipo, message.Valor, message.Origen, message.Destino, message.Hora}}

	for _, value := range data {
		err := writer.Write(value)
		checkError("Cannot write to file", err)
	}

	log.Printf("mensaje del cliente IDPaquete: %s", message.IDPaquete)
	log.Printf("mensaje del cliente Estado: %s", message.Estado)
	log.Printf("mensaje del cliente Intento: %s", message.Intento)

	return message, nil
}

func (s *Server) SayHello(ctx context.Context, message *PaqueteLogistica) (*PaqueteLogistica, error) {
	log.Printf("mensaje del cliente Destino: %s", message.IDPaquete)
	return &PaqueteLogistica{Estado: "arreglar ordenes.go"}, nil
}
