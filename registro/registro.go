package registro

import (
	"log"

	"golang.org/x/net/context"
)

type Server struct {
}

func (s *Server) SayHello(ctx context.Context, message *Ficha) (*Ficha, error) {
	log.Printf("mensaje del cliente Destino: %s", message.Origen)
	return &Ficha{Origen: "Santiago, Chile", Destino: "Tacna, Perú"}, nil
}
