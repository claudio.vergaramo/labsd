package ordenes

import (
	context "context"
	"encoding/csv"
	"log"
	"os"
	"time"
)

type Server struct {
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

//Seguimiento Hola
func (s *Server) Seguimiento(ctx context.Context, message *OrdenCliente) (*OrdenCliente, error) {
	var id string = message.Id
	var producto string = message.Producto
	var tienda string = message.Tienda
	var destino string = message.Destino
	var valor string = message.Valor
	var prioritario string = message.Tipo
	var seguimiento string = message.Seguimiento
	//var anno string
	var timestamp string
	t := time.Now()
	timestamp = t.Format("2006-01-02 15:04:05")

	if message.Tienda != "pyme" {
		seguimiento = "0"

	} else {
		seguimiento = id + t.Format("20060102150405")
	}
	log.Printf("%s", seguimiento)

	//Crear CSV

	file, err := os.OpenFile("../logistica/registro_ordenes.csv", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	checkError("Cannot create file", err)
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	var data = [][]string{{timestamp, id, prioritario, producto, valor, tienda, destino, seguimiento}}

	for _, value := range data {
		err := writer.Write(value)
		checkError("Cannot write to file", err)
	}
	return &OrdenCliente{Seguimiento: seguimiento}, nil
}

func (s *Server) SayHello(ctx context.Context, message *OrdenCliente) (*OrdenCliente, error) {

	log.Printf("mensaje del cliente Destino: %s", message.Destino)
	return &OrdenCliente{Destino: "arreglar ordenes.go"}, nil
}
