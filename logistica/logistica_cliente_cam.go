package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/streadway/amqp"
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	paquetes "gitlab.com/claudio.vergaramo/labsd/paquetes"
)

func read(tipoDeCliente string) ([][]string, [][]string, [][]string) {

	csvfile, err := os.Open(tipoDeCliente + ".csv")
	r := csv.NewReader(csvfile)

	lines, err := r.ReadAll()
	if err != nil {
		log.Fatalf("error reading all lines: %v", err)
	}

	timestamp := make([]string, len(lines))
	id := make([]string, len(lines))
	producto := make([]string, len(lines))
	valor := make([]string, len(lines))
	tienda := make([]string, len(lines))
	destino := make([]string, len(lines))
	prioritario := make([]string, len(lines))
	seguimiento := make([]string, len(lines))

	var retail [][]string
	var priorit [][]string
	var normal [][]string

	for i, line := range lines {
		timestamp[i] = line[0]
		id[i] = line[1]
		prioritario[i] = line[2]
		producto[i] = line[3]
		valor[i] = line[4]
		tienda[i] = line[5]
		destino[i] = line[6]
		seguimiento[i] = line[7]

		data := []string{id[i], prioritario[i], valor[i], seguimiento[i], "0", "En Bodega", tienda[i], destino[i]}

		if tienda[i] != "pyme" {
			retail = append(retail, data)
		} else {
			if line[2] == "1" {
				priorit = append(priorit, data)
			} else {
				normal = append(normal, data)
			}
		}
	}
	return retail, priorit, normal
}

func EnvioPaquete(conn *grpc.ClientConn, lista []string) {
	c := paquetes.NewPaquetesLogisticaServiceClient(conn)
	message := paquetes.PaqueteLogistica{
		IDPaquete:   lista[0],
		Tipo:        lista[1],
		Valor:       lista[2],
		Seguimiento: lista[3],
		Intento:     lista[4],
		Estado:      lista[5],
		Origen:      lista[6],
		Destino:     lista[7],
		Hora:        "00:00:00",
	}
	response, err := c.ListaEnvio(context.Background(), &message)
	if err != nil {
		log.Fatalf("Error llamando a ListaEnvio: %s", err)
	}
	log.Printf("Respuesta del Server Destino: %s", response.Hora)

	//DEFINIR TIempo de paquetes retail
	dur := time.Duration(5) * time.Second
	fmt.Printf("Sleeping for %v\n", dur)
	time.Sleep(dur)

}

func main() {
	//var conn *grpc.ClientConn
	conn, err := grpc.Dial(":9002", grpc.WithInsecure())

	fmt.Println("¿Cada cuantos segundos quiere entregar paquetes?:")
	var tiempito int32
	fmt.Scanln(&tiempito)

	if err != nil {
		log.Fatalf("Se casho el sistema: %s", err)
	}

	//st := paquetes.NewPaquetesLogisticaServiceClient(conn)

	defer conn.Close()
	retail, priorit, normal := read("registro_ordenes")

	fmt.Printf("%v", retail)
	fmt.Printf("%v", priorit)
	fmt.Printf("%v", normal)

	suma := len(retail) + len(priorit) + len(normal)

	for i := 0; i <= suma; i++ {
		if len(retail) >= 1 {
			dur := time.Duration(tiempito) * time.Second
			fmt.Printf("Entregando Paquete por %v\n", dur)
			time.Sleep(dur)

			go EnvioPaquete(conn, retail[0])

			retail = retail[1:]

		} else if len(priorit) >= 1 {
			dur := time.Duration(tiempito) * time.Second
			fmt.Printf("Entregando Paquete por %v\n", dur)
			time.Sleep(dur)

			go EnvioPaquete(conn, priorit[0])
			priorit = priorit[1:]

		} else if len(normal) >= 1 {
			dur := time.Duration(tiempito) * time.Second
			fmt.Printf("Entregando Paquete por %v\n", dur)
			time.Sleep(dur)

			go EnvioPaquete(conn, normal[0])
			normal = normal[1:]

		}
	}
	dur := time.Duration(4) * time.Second
	fmt.Printf("Sleeping for %v\n", dur)
	time.Sleep(dur)

	conn, err := amqp.Dial("amqp://mqadminuser:mqadminpassword@dist123:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"TestQueue",
		false,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	fmt.Println(q)

	err = ch.Publish(
		"",
		"TestQueue",
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte("Hello World"),
		},
	)

	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	fmt.Println("Mensaje publicado correctamente en la Queue")
}
