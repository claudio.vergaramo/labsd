package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/grpc"

	ordenes "gitlab.com/claudio.vergaramo/labsd/ordenes"
)

func readRetail(tipoDeCliente string) ([]string, []string, []string, []string, []string) {

	csvfile, err := os.Open(tipoDeCliente + ".csv")
	r := csv.NewReader(csvfile)

	lines, err := r.ReadAll()
	if err != nil {
		log.Fatalf("error reading all lines: %v", err)
	}

	id := make([]string, len(lines)-1)
	producto := make([]string, len(lines)-1)
	valor := make([]string, len(lines)-1)
	tienda := make([]string, len(lines)-1)
	destino := make([]string, len(lines)-1)

	for i, line := range lines {
		if i == 0 {
			// skip header line
			continue
		}
		id[i-1] = line[0]
		producto[i-1] = line[1]
		valor[i-1] = line[2]
		tienda[i-1] = line[3]
		destino[i-1] = line[4]
	}

	return id, producto, valor, tienda, destino
}

func read(tipoDeCliente string) ([]string, []string, []string, []string, []string, []string) {

	csvfile, err := os.Open(tipoDeCliente + ".csv")
	r := csv.NewReader(csvfile)

	lines, err := r.ReadAll()
	if err != nil {
		log.Fatalf("error reading all lines: %v", err)
	}

	id := make([]string, len(lines)-1)
	producto := make([]string, len(lines)-1)
	valor := make([]string, len(lines)-1)
	tienda := make([]string, len(lines)-1)
	destino := make([]string, len(lines)-1)
	prioritario := make([]string, len(lines)-1)

	for i, line := range lines {
		if i == 0 {
			// skip header line
			continue
		}
		id[i-1] = line[0]
		producto[i-1] = line[1]
		valor[i-1] = line[2]
		tienda[i-1] = line[3]
		destino[i-1] = line[4]
		prioritario[i-1] = line[5]
	}

	return id, producto, valor, tienda, destino, prioritario
}

func main() {
	//var conn *grpc.ClientConn

	fmt.Println("Escribe tu tipo de tienda (pymes o retail):")
	var tdcliente string
	fmt.Scanln(&tdcliente)

	fmt.Println("Cada cuantos segundos quiere enviar ordenes?:")
	var tiempito int
	fmt.Scanln(&tiempito)

	conn, err := grpc.Dial(":9001", grpc.WithInsecure())

	if err != nil {
		log.Fatalf("Se casho el sistema: %s", err)
	}

	defer conn.Close()

	c := ordenes.NewOrdenesClienteServiceClient(conn)
	if tdcliente == "pymes" {
		id, producto, valor, tienda, destino, prioritario := read(tdcliente)

		for i := range id {
			{
				message := ordenes.OrdenCliente{
					Id:          id[i],
					Producto:    producto[i],
					Valor:       valor[i],
					Tienda:      tienda[i],
					Destino:     destino[i],
					Tipo:        prioritario[i],
					Seguimiento: "0",
				}

				response, err := c.Seguimiento(context.Background(), &message)

				if err != nil {
					log.Fatalf("Error llamando a RutinaOrdenes: %s", err)
				}
				log.Printf("Respuesta del Server Seguimiento: %s", response.Seguimiento)

				dur := time.Duration(tiempito) * time.Second
				fmt.Printf("Sleeping for %v\n", dur)
				time.Sleep(dur)
			}
		}
	} else {
		id, producto, valor, tienda, destino := readRetail(tdcliente)

		for i := range id {
			{
				message := ordenes.OrdenCliente{
					Id:          id[i],
					Producto:    producto[i],
					Valor:       valor[i],
					Tienda:      tienda[i],
					Destino:     destino[i],
					Tipo:        "0",
					Seguimiento: "No aplica",
				}

				response, err := c.Seguimiento(context.Background(), &message)

				if err != nil {
					log.Fatalf("Error llamando a RutinaOrdenes: %s", err)
				}
				log.Printf("Respuesta del Server Seguimiento: %s", response.Seguimiento)

				dur := time.Duration(tiempito) * time.Second
				fmt.Printf("Sleeping for %v\n", dur)
				time.Sleep(dur)
			}
		}
	}
}
