package main

import (
	"log"
	"net"

	"gitlab.com/claudio.vergaramo/labsd/paquetes"
	"google.golang.org/grpc"
)

func main() {
	lis, err := net.Listen("tcp", ":9002")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := paquetes.Server{}

	grpcServer := grpc.NewServer()

	paquetes.RegisterPaquetesLogisticaServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Falla del puerto gRPC 9002, se casho el sistema: %v", err)
	}

}
