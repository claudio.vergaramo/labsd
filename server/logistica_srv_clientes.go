package main

import (
	"log"
	"net"

	"gitlab.com/claudio.vergaramo/labsd/ordenes"
	"google.golang.org/grpc"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	lis, err := net.Listen("tcp", ":9001")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := ordenes.Server{}

	grpcServer := grpc.NewServer()

	ordenes.RegisterOrdenesClienteServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Falla del puerto gRPC 9001, se casho el sistema: %v", err)
	}

}
